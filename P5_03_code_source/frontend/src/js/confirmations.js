//Recuperation du panier et injection de la vue
let orders = localStorage.getItem('orderResponses');
if (orders === null) {
    window.location.href = "./index.html";
} else {
    orders = JSON.parse(orders);
}

//On génère le visuel
generateVisual(orders);

/*
* Ajoute le contenu de l'article sur la page d'accueil
*/
function generateVisual(orders) {
    //On génère la carte de chaque commande
    orders.forEach(order => {
        document.getElementById("commands").innerHTML += getCardAsHtml(order);
    })

    //On calcule le prix total de l'ensemble des commandes
    let allPrice = 0;
    orders.forEach(order => {
        order.products.forEach(product => {
            allPrice += product.price;
        });
    });
    //On remplace le prix total affiché par défaut
    document.getElementById("totalPrice").innerHTML = "Total de votre commande: " + (allPrice / 1000).toFixed(2) + "€";
}

/*
* Retourne sous forme de chaine le contenu HTML à ajouter dans le main en fonction du meuble choisi
*/
function getCardAsHtml(order) {

    let articles = [];
    //Construction liste de produit et prix/produit
    order.products.forEach(product => {
        let existingProduct = articles.find(o => o.id === product._id);
        if (existingProduct === undefined) {
            articles.push(
                {
                    id: product._id,
                    name: product.name,
                    quantity: 1,
                    price: product.price,
                    totalPrice: product.price,
                }
            );
        } else {
            //Récupération de l'index
            const key = articles.findIndex(p => p === existingProduct)

            //On incrémente le nombre de produits et le prix total
            articles[key].quantity += 1;
            articles[key].totalPrice += articles[key].price;
        }
    });

    //On défini le prix de toute les commandes réunies
    let totalPrice = 0;
    articles.forEach(article => {
        totalPrice += article.totalPrice;
    })

    //On genere la liste des articles
    let listArticles = "<ul>";
    articles.forEach(article => {
        listArticles += "<li>" + article.quantity + "x " + article.name + "</li>";
    })
    listArticles += "</ul>";

    console.log(articles, totalPrice);

    return '<article>\n' +
        '       <h2>Commande ' + order.orderId + '</h2>\n' +
        listArticles +
        '       <p>Total: ' + (totalPrice / 1000).toFixed(2) + '€</p>\n' +
        '   </article>'
}

MyAPI.get(type + "/").then(validateData).then(generateVisual);

function generateVisual(furnitures) {
    //On vérifie l'intégrité des données
    if (furnitures instanceof Error) {
        console.error(furnitures);
        document.querySelector("#products").innerHTML += "<div>Une erreur est survenue, merci de réessayer plus tard!</div>";
        return;
    }
    if (furnitures.length < 1) {
        document.querySelector("#products").innerHTML += "<div>Aucun produits disponibles!</div>";
        return;
    }

    //Si tableau non vide et données conformes, on parcours
    furnitures.forEach(furniture => {
        document.querySelector("#products").innerHTML += getCardAsHtml(furniture);
    });
}

function validateData(furnitures) {
    /*Vérification que l'entrée soit un tableau non vide*/
    if (!furnitures instanceof Array) {
        return new Error("API Result is not an array");
    }

    /*Vérification de chaque entrée et suppression de l'entrée si non conforme*/
    return furnitures.filter(furniture => {
        if (furniture.description === undefined) {
            console.error("Pas de propriété description", furniture);
            return false;
        }
        if (furniture._id === undefined) {
            console.error("Pas de propriété _id", furniture);
            return false;
        }
        if (furniture.imageUrl === undefined) {
            console.error("Pas de propriété imageUrl", furniture);
            return false;
        } else {
            //On vérifie que l'URL retourne bien un code 200
            const http = new XMLHttpRequest();
            http.open('HEAD', furniture.imageUrl, false);
            http.send();
            if (http.status !== 200) {
                console.error("Impossible d'accéder à l'image", furniture);
                return false;
            }
        }

        if (furniture.name === undefined) {
            console.error("Pas de propriété name", furniture);
            return false;
        }
        if (furniture.price === undefined) {
            console.error("Pas de propriété price", furniture);
            return false;
        }
        return true;
    });
}

function getCardAsHtml(furniture) {
    return '<figure>\n' +
        '            <img alt="Image du meuble ' + furniture.name + '" src="' + furniture.imageUrl + '">\n' +
        '            <div class="actions">\n' +
        '                <a aria-label="Voir le produit ' + furniture.name + '" href="./product.html?id=' + furniture._id + '&type=' + type + '">\n' +
        '                    <!--Icône loupe-->\n' +
        '                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24"\n' +
        '                         xmlns="http://www.w3.org/2000/svg">\n' +
        '                        <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" stroke-linecap="round"\n' +
        '                              stroke-linejoin="round"\n' +
        '                              stroke-width="2"></path>\n' +
        '                    </svg>\n' +
        '                </a>\n' +
        '            </div>\n' +
        '            <figcaption>\n' +
        '                <div class="head">\n' +
        '                    <h3>' + furniture.name + '</h3>\n' +
        '                    <span>' + (furniture.price / 1000).toFixed(2) + '€</span>\n' +
        '                </div>\n' +
        '                <div class="foot">\n' +
        '                    <span>En stock</span>\n' +
        '                    <p>' + furniture.description + '</p>\n' +
        '                </div>\n' +
        '            </figcaption>\n' +
        '        </figure>';
}
function API(cnx) {
    this.cnx = cnx;

    /*Method GET*/
    this.get = async (url = '/') => {
        try {
            const res = await fetch(this.cnx + url);
            if (!res.ok) {
                // noinspection ExceptionCaughtLocallyJS
                throw new Error(res.statusText);
            }
            return await res.json();
        } catch (error) {
            throw error
        }
    };

    /*Method POST*/
    this.post = async (url = '/', opts = {}) => {
        try {
            const res = await fetch(this.cnx + url, {
                method: 'POST',
                body: JSON.stringify(opts),
                headers: {"Content-type": "application/json; charset=UTF-8"}
            });
            if (!res.ok) {
                // noinspection ExceptionCaughtLocallyJS
                throw new Error(res.statusText);
            }
            return await res.json();
        } catch (error) {
            throw error;
        }
    }
}

/*Choix en fonction des paramètres de l'url*/
const urlParams = new URLSearchParams(window.location.search);

//Choix des objets à afficher
let type;
switch (urlParams.get('type')) {
    case 'teddies':
        type = 'teddies';
        break;
    case 'cameras':
        type = 'cameras';
        break;
    default:
        type = 'furniture';
        break;
}

//Changement de la bannière en fonction du type si hero banner présente
if (document.querySelector("#hero") !== null) {
    switch (type) {
        case 'teddies':
            document.querySelector("#hero h1").innerHTML = "Ours en peluche";
            document.querySelector("#hero").classList.add('teddies');
            break;
        case 'cameras':
            document.querySelector("#hero h1").innerHTML = "Caméras";
            document.querySelector("#hero").classList.add('cameras');
            break;
        default:
            document.querySelector("#hero h1").innerHTML = "Mobilier";
            document.querySelector("#hero").classList.add('furniture');
            break;
    }
}

//Ajout nombre d'article dans le badge
function updateCounter() {
    if (localStorage.getItem('cart') !== null) {
        let count = 0;
        JSON.parse(localStorage.getItem('cart')).forEach(i => {
            count += i.quantity
        });
        document.getElementById("articles-count").innerHTML = count;
    }
}

updateCounter();
//Création de MyApi
const MyAPI = new API("http://localhost:3000/api/");

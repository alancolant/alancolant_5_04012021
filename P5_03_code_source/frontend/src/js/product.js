//On redirige si pas d'id dans les query parameters
if (urlParams.get('id') === null) {
    window.location.href = "./index.html";
}

const id = urlParams.get('id');

//Récupération des informations et génération du visuel
MyAPI.get(type + "/" + id).then(generateVisual).then(waitClick);


/*
* Ajoute un élément au panier à l'envoi du formulaire
*/
function waitClick(furniture) {
    const form = document.querySelector('#add_to_shopping_cart');
    form.addEventListener('submit', event => {
        event.preventDefault();
        const formData = new FormData(form);

        //Création localstorage si existant et récupération
        let cart = localStorage.getItem('cart');
        if (cart === null) {
            cart = [];
        } else {
            cart = JSON.parse(cart)
        }

        //On cherche si une entrée existe déja pour ce produit
        let existingProduct = cart.find(o => o.id === id && o.options === formData.get('options') && o.type === type);
        if (existingProduct === undefined) {
            cart.push(
                {
                    id: id,
                    type: type,
                    options: formData.get('options'),
                    name: furniture.name,
                    imageUrl: furniture.imageUrl,
                    quantity: parseInt(formData.get("quantity")),
                    price: furniture.price
                }
            );
        } else {
            //Récupération de l'index
            const key = cart.findIndex(p => p === existingProduct)

            //On incrémente le nombre de produits
            cart[key].quantity += parseInt(formData.get('quantity'));
        }

        //Stockage dans localstorage
        localStorage.setItem('cart', JSON.stringify(cart))

        //Changement du contenu du bouton pendant 3secondes
        document.querySelector('form button').innerHTML = 'Ajouté';
        setTimeout(
            function () {
                document.querySelector('form button').innerHTML = 'Ajouter au panier';
            }, 3000);

        //Mise à jour du badge de comptage panier
        updateCounter();
    });
}

/*
* Ajoute le contenu de l'article sur la page d'accueil
*/
function generateVisual(furniture) {
    //On vérifie l'intégrité des données
    if (furniture instanceof Error) {
        console.error(furniture);
        document.querySelector("#products").innerHTML += "<div>Une erreur est survenue, merci de réessayer plus tard!</div>";
        return;
    }

    document.querySelector("#main").innerHTML = getCardAsHtml(furniture);
    return furniture
}

/*
* Retourne sous forme de chaine le contenu HTML à ajouter dans le main en fonction du meuble choisi
*/
function getCardAsHtml(furniture) {

    //Création des options de vernis
    let options = '';
    let label;

    if (type === 'furniture') {
        label = '<label for="options">Vernis</label>';
        furniture.varnish.forEach(option => {
            options += '<option value="' + option + '">' + option + '</option>';
        })
    } else if (type === 'teddies') {
        label = '<label for="options">Couleur</label>';
        furniture.colors.forEach(option => {
            options += '<option value="' + option + '">' + option + '</option>';
        })
    } else if (type === 'cameras') {
        label = '<label for="options">Lentilles</label>';
        furniture.lenses.forEach(option => {
            options += '<option value="' + option + '">' + option + '</option>';
        })
    }


    return '<figure id="product">\n' +
        '        <img alt="Image du produit ' + furniture.name + '" src="' + furniture.imageUrl + '">\n' +
        '        <figcaption>\n' +
        '            <div class="top">\n' +
        '                <h2>' + furniture.name + '</h2>\n' +
        '                <span>' + (furniture.price / 1000).toFixed(2) + '€</span>\n' +
        '            </div>\n' +
        '            <div class="bottom">\n' +
        '                <div class="availability">\n' +
        '                    <p>Disponibilité:</p>\n' +
        '                    <span>En stock</span>\n' +
        '                </div>\n' +
        '                <p class="description">\n' + furniture.description + '</p>\n' +
        '                <!--Formulaire de choix quantité, couleur-->\n' +
        '                <form id="add_to_shopping_cart">\n' +
        '                    <div class="choice">\n' + label +
        '                        <select name="options">\n' + options + '</select>\n' +
        '                    </div>\n' +
        '                    <div class="quantity">\n' +
        '                        <label for="quantity">Quantité</label>\n' +
        '                        <input id="quantity" name="quantity" type="number" value="1"/>\n' +
        '                    </div>\n' +
        '                    <button type="submit">Ajouter au panier</button>\n' +
        '                </form>\n' +
        '            </div>\n' +
        '        </figcaption>\n' +
        '        <!--Javascript insertion-->\n' +
        '    </figure>';
}
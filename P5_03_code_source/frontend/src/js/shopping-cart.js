//Recuperation du panier et injection de la vue
let cart = localStorage.getItem('cart');
if (cart === null) {
    cart = [];
} else {
    cart = JSON.parse(cart)
}
//Si le panier est vide on modifie le visuel
if (cart.length < 1) {
    document.getElementById("shopping-cart").innerHTML = "<h1>Vous n'avez pas d'articles dans votre panier</h1>"
}
//Aussi non on importe le panier
else {
    generateVisual(cart);
    listenFormSubmit();
}

/*
* Ajoute le contenu de l'article sur la page d'accueil
*/
function generateVisual(cart) {
    //On vérifie l'intégrité des données
    cart.forEach(item => {
        document.getElementById("articles").innerHTML += getCardAsHtml(item);
    })
}

/*
* Retourne sous forme de chaine le contenu HTML à ajouter dans le main en fonction du meuble choisi
*/
function getCardAsHtml(item) {
    return '<tr data-option="' + item.options + '" data-id="' + item.id + '">\n' +
        '       <td>\n' +
        '           <button aria-label="Supprimer l\'article" onclick="deleteArticle(this)">\n' +
        '               <svg fill="none" stroke="currentColor" viewBox="0 0 24 24"\n' +
        '                    xmlns="http://www.w3.org/2000/svg">\n' +
        '                   <path d="M6 18L18 6M6 6l12 12" stroke-linecap="round" stroke-linejoin="round"\n' +
        '                         stroke-width="2"/>\n' +
        '               </svg>\n' +
        '           </button>\n' +
        '       </td>\n' +
        '       <td>\n' +
        '           <img alt="Image du produit ' + item.name + '" src="' + item.imageUrl + '">\n' +
        '           <p>\n' +
        '              ' + item.name + '\n' +
        '           </p>\n' +
        '           <span>\n' +
        '              (' + item.options + ')\n' +
        '           </span>\n' +
        '       </td>\n' +
        '       <td>\n' +
        '           <label class="sr-only" for="number">Nombre d\'article</label>\n' +
        '           <input onchange="updateQuantity(this)" type="number" value="' + item.quantity + '">\n' +
        '       </td>\n' +
        '       <td id="price">' + (item.quantity * item.price / 1000).toFixed(2) + '€</td>\n' +
        '   </tr>';
}

/*
*Permet de supprimer un élément du panier et du localStorage
*/
function deleteArticle(item) {
    //Récupération informations de l'article
    let element_option = item.parentNode.parentNode.dataset.option
    let element_id = item.parentNode.parentNode.dataset.id

    //Filtrage du tableau pour suppression de l'entrée
    cart = cart.filter(product => {
        return !(product.options === element_option && product.id === element_id);
    });

    //Supression ligne du tableau correspondante
    item.parentNode.parentNode.outerHTML = "";

    //Màj du panier dans localStorage
    localStorage.setItem('cart', JSON.stringify(cart));

    //Mise à jour du badge de comptage panier
    updateCounter();
}

/*
*Permet de supprimer un élément du panier et du localStorage
*/
function updateQuantity(item) {
    //Récupération informations de l'article
    const new_value = item.value;
    const element_option = item.parentNode.parentNode.dataset.option
    const element_id = item.parentNode.parentNode.dataset.id

    //Si la nouvelle valeur est zéro, on supprime l'entrée
    if (new_value < 1) {
        deleteArticle(item);
    }
    //Aussi non on modifie la quantité associée
    else {
        //Récupération clé correspondante
        const product = cart.find(o => o.id === element_id && o.options === element_option);
        const key = cart.findIndex(p => p === product)

        //Enregistrement nouvelle valeur
        cart[key].quantity = parseInt(new_value);

        //Modification du prix
        item.parentNode.parentNode.querySelector("#price").innerHTML = (cart[key].quantity * cart[key].price / 1000).toFixed(2) + '€';

        //Màj du panier dans localStorage
        localStorage.setItem('cart', JSON.stringify(cart));
    }

    //Mise à jour du badge de comptage panier
    updateCounter();
}

/*
*Envoi de l'ordre de paiement à l'API à la soumission du formulaire
*/
function listenFormSubmit() {
    const form = document.querySelector('#orderingForm');
    form.addEventListener('submit', event => {
        event.preventDefault();
        formDatas = new FormData(form);

        //On retourne si formulaire non valide
        if (!verifyFormDatas(formDatas)) {
            return;
        }

        //Regroupement par type et récupération de l'ID uniquement
        let groupedCart = cart.reduce((res, product) => {
            //On ajoute x fois le produit dans le tableau retourné
            for (let i = 0; i < product.quantity; i++) {
                res[product.type] = [...res[product.type] || [], product.id];
            }
            return res;
        }, {});

        //Création de l'objet à envoyer
        let datas = {
            firstName: formDatas.get('firstName'),
            lastName: formDatas.get('lastName'),
            address: formDatas.get('address'),
            city: formDatas.get('city'),
            email: formDatas.get('email'),
        }

        //Envoi des requêtes
        let i = 0; //Nombre de requêtes envoyées (incrementées dans les conditions)
        if (groupedCart.furniture !== undefined) {
            i++;
            MyAPI.post('furniture/order', {
                contact: datas,
                products: groupedCart.furniture
            }).then(response => {
                addResponse('furniture', response);
            });
        }
        if (groupedCart.teddies !== undefined) {
            i++;
            MyAPI.post('teddies/order', {
                contact: datas,
                products: groupedCart.teddies
            }).then(response => {
                addResponse('teddies', response);
            });
        }
        if (groupedCart.cameras !== undefined) {
            i++;
            MyAPI.post('cameras/order', {
                contact: datas,
                products: groupedCart.cameras
            }).then(response => {
                addResponse('cameras', response);
            });
        }

        let responses = [];

        /*
        Fonction appelés au retour de l'API
         */
        function addResponse(type, response) {
            responses.push(response);
            //Si on a récupéré TOUTES les requêtes
            if (responses.length === i) {
                //Suppression du panier
                cart = [];
                localStorage.removeItem('cart');
                //Enregistrement des réponses et redirection
                localStorage.setItem('orderResponses', JSON.stringify(responses));
                window.location.href = './confirmations.html';
            }
        }


    });
}


/*
*Vérifie le formulaire et affiche les erreurs
*/
function verifyFormDatas(formDatas) {
    //Définition des regex
    const numericRegex = /\d+/;
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    let errorsCount = 0;

    //Vérification prénom
    if (formDatas.get('firstName') === '') {
        errorsCount++;
        setError('firstName', 'Le champ prénom est obligatoire!');
    } else if (formDatas.get('firstName').length > 50) {
        errorsCount++;
        setError('firstName', 'Le champ prénom est trop long!');
    } else if (numericRegex.test(formDatas.get('firstName'))) {
        errorsCount++;
        setError('firstName', 'Le champ prénom ne doit pas contenir de chiffres!');
    }

    //Vérification nom
    if (formDatas.get('lastName') === '') {
        errorsCount++;
        setError('lastName', 'Le champ nom est obligatoire!');
    } else if (formDatas.get('lastName').length > 50) {
        errorsCount++;
        setError('lastName', 'Le champ nom est trop long!');
    } else if (numericRegex.test(formDatas.get('lastName'))) {
        errorsCount++;
        setError('lastName', 'Le champ nom ne doit pas contenir de chiffres!');
    }

    //Vérification adresse
    if (formDatas.get('address') === '') {
        errorsCount++;
        setError('address', 'Le champ adresse est obligatoire!');
    }

    //Vérification ville
    if (formDatas.get('city') === '') {
        errorsCount++;
        setError('city', 'Le champ ville est obligatoire!');
    } else if (formDatas.get('city').length > 78) {
        errorsCount++;
        setError('city', 'Le champ ville est trop long!');
    }

    //Vérification email
    if (formDatas.get('email') === '') {
        return setError('email', 'Le champ email est obligatoire!');
    } else if (!emailRegex.test(formDatas.get('email'))) {
        return setError('email', 'Le champ email n\'est pas correct!');
    }

    //Retourne la réponse de la fonction
    return errorsCount === 0;
}

/*
*Ajoute l'erreur dans le formulaire et crée une console log
*/
function setError(field, message) {
    document.querySelector('span[data-for=' + field + ']').innerHTML = message;
    console.error(new Error(message));
}
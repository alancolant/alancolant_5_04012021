# Orinoco #

Il s'agit du code source pour le site Orinoco comprenant le backend et le frontend

### Prerequisites ###

You will need to have Node and `npm` installed locally on your machine. For developers, you need to have SCSS
Pre-compilator installed

### Installation ###

# Back-End

Clone the backend folder. From within the folder, run `npm install`. You can then run the server with `node server`. The
server should run on `localhost` with default port `3000`. If the server runs on another port for any reason, this is
printed to the console when the server starts, e.g. `Listening on port 3001`.

# Front-End

Clone the frontend folder, you can now access website by index.html